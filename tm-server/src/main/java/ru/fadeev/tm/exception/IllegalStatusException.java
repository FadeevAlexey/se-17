package ru.fadeev.tm.exception;

import org.jetbrains.annotations.Nullable;

public final class IllegalStatusException extends RuntimeException {

    public IllegalStatusException(@Nullable final String message) {
        super(message);
    }

}
