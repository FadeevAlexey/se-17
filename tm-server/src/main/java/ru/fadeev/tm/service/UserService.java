package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IUserRepository;
import ru.fadeev.tm.api.service.IUserService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.User;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    public UserService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<User> findAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @NotNull final List<User> users = repository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return users;
    }

    @Override
    @Nullable
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final User user) throws Exception {
        if (user == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        repository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return false;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        final boolean isLoginExist = repository.isLoginExist(login);
        entityManager.getTransaction().commit();
        entityManager.close();
        return isLoginExist;
    }

    @Override
    @Nullable
    public User findUserByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository repository = new UserRepository(entityManager);
        @Nullable final User user = repository.findUserByLogin(login);
        entityManager.getTransaction().commit();
        entityManager.close();
        return user;
    }

    @Override
    public void setAdminRole(@Nullable final User user) throws Exception {
        if (user == null) return;
        user.setRole(Role.ADMINISTRATOR);
        merge(user);
    }

}