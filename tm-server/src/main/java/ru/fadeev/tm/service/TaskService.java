package ru.fadeev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.api.service.ITaskService;
import ru.fadeev.tm.api.service.ServiceLocator;
import ru.fadeev.tm.entity.Task;
import ru.fadeev.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import java.util.*;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    public TaskService(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @NotNull List<Task> findAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final List<Task> tasks = repository.findAll();
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Override
    public void persist(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findOne(id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String userId, final String id) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (id == null || id.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final Task task = repository.findOneByUserId(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
        return task;
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeByIdTask(userId, taskId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(@Nullable final Task task) throws Exception {
        if (task == null) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.findAllByUserId(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@Nullable final String projectId, @Nullable final String userId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.findAllByProjectId(projectId, userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @Nullable
    @Override
    public String findIdByName(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) return null;
        if (userId == null || userId.isEmpty()) return null;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @Nullable final String id = repository.findIdByName(userId, name);
        entityManager.getTransaction().commit();
        entityManager.close();
        return id;
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAllProjectTask(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return;
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        repository.removeAllProjectTask(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.searchByName(userId, string);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@Nullable final String userId, @Nullable final String string) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (string == null || string.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.searchByDescription(userId, string);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStartDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.sortAllByStartDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByFinishDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.sortAllByFinishDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByStatus(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.sortAllByStatus(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public Collection<Task> sortByCreationDate(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final EntityManager entityManager = serviceLocator.getSqlSessionService().getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository repository = new TaskRepository(entityManager);
        @NotNull final Collection<Task> tasks = repository.sortAllByCreationDate(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
        return tasks;
    }

}