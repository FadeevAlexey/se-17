package ru.fadeev.tm.listener;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import javax.persistence.*;
import java.util.Date;

import static ru.fadeev.tm.util.EntityUtil.*;

public final class EntityListener {

    @PrePersist
    public void prePersist(@NotNull final Object object) throws JMSException {
        @NotNull final String log = String.format(
                "Method PrePersist Invoked Upon Entity :: %s. Time :: %s\n",
                objectToJson(object), new Date(System.currentTimeMillis())
        );
        sendMessage(log, object.getClass().getSimpleName());
    }

    @PostPersist
    public void postPersist(@NotNull final Object object) throws JMSException {
        @NotNull final String log = String.format(
                "Method PostPersist Invoked Upon Entity :: %s. Time :: %s\n",
                objectToJson(object), new Date(System.currentTimeMillis())
        );
        sendMessage(log, object.getClass().getSimpleName());
    }

    @PreRemove
    public void PreRemove(@NotNull final Object object) throws JMSException {
        @NotNull final String log = String.format(
                "Method PreRemove Invoked Upon Entity :: %s. Time :: %s\n",
                objectToJson(object), new Date(System.currentTimeMillis())
        );
        sendMessage(log, object.getClass().getSimpleName());
    }

    @PostRemove
    public void PostRemove(@NotNull final Object object) throws JMSException {
        @NotNull final String log = String.format(
                "Method PostRemove Invoked Upon Entity :: %s. Time :: %s\n",
                objectToJson(object), new Date(System.currentTimeMillis())
        );
        sendMessage(log, object.getClass().getSimpleName());
    }


    @PreUpdate
    public void PreUpdate(@NotNull final Object object) throws JMSException {
        @NotNull final String log = String.format(
                "Method PreUpdate Invoked Upon Entity :: %s. Time :: %s\n",
                objectToJson(object), new Date(System.currentTimeMillis())
        );
        sendMessage(log, object.getClass().getSimpleName());
    }

    @PostUpdate
    public void PostUpdate(@NotNull final Object object) throws JMSException {
        @NotNull final String log = String.format(
                "Method PostUpdate Invoked Upon Entity ::: %s. Time :: %s\n",
                objectToJson(object), new Date(System.currentTimeMillis())
        );
        sendMessage(log, object.getClass().getSimpleName());
    }

    private void sendMessage(@NotNull final String log, @NotNull final String topicName) throws JMSException {
        @NotNull final ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(ActiveMQConnectionFactory.DEFAULT_BROKER_BIND_URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic(topicName);
        @NotNull final MessageProducer producer = session.createProducer(destination);
        @NotNull final TextMessage message = session.createTextMessage(log);
        producer.send(message);
        connection.close();
    }

}