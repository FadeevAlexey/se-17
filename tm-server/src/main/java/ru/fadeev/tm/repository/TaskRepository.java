package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ITaskRepository;
import ru.fadeev.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collection;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    @NotNull
    private EntityManager entityManager;

    public TaskRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public void persist(@NotNull final Task task) {
        entityManager.persist(task);
    }

    @Override
    public void merge(@NotNull final Task task) {
        entityManager.merge(task);
    }

    @Override
    public void removeAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Task> cq = cb.createCriteriaDelete(Task.class);
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        @NotNull final CriteriaQuery<Task> all = cq.select(rootEntry);
        TypedQuery<Task> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Task> cq = cb.createCriteriaDelete(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(cb.equal(rootEntry.get("id"), id));
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        @NotNull final CriteriaQuery<Task> query = cq.select(rootEntry);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        TypedQuery<Task> allQuery = entityManager.createQuery(query);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId, @NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        @NotNull final CriteriaQuery<Task> query = cq.select(rootEntry);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("project").get("id"), projectId)
        );
        TypedQuery<Task> allQuery = entityManager.createQuery(query);
        return allQuery.getResultList();
    }

    @Nullable
    @Override
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        @NotNull final CriteriaQuery<Task> query = cq.select(rootEntry);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("id"), id)
        );
        TypedQuery<Task> allQuery = entityManager.createQuery(query);
        return allQuery.getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Task> cq = cb.createCriteriaDelete(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        entityManager.createQuery(cq).executeUpdate();
    }

    @Nullable
    @Override
    public String findIdByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        @NotNull final CriteriaQuery<Task> query = cq.select(rootEntry);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("name"), name)
        );
        TypedQuery<Task> allQuery = entityManager.createQuery(query);
        @Nullable final Task task = allQuery.getResultStream()
                .findFirst()
                .orElse(null);
        return task == null ? null : task.getId();
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Task> cq = cb.createCriteriaDelete(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("project").get("id"), projectId)
        );
        entityManager.createQuery(cq).executeUpdate();
    }

    @Override
    public void removeAllProjectTask(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Task> cq = cb.createCriteriaDelete(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.isNotNull(rootEntry.get("project"))
        );
        entityManager.createQuery(cq).executeUpdate();
    }

    @Override
    public void removeByIdTask(@NotNull final String userId, @NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Task> cq = cb.createCriteriaDelete(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.equal(rootEntry.get("id"), id)
        );
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByStartDate(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("startDate")));
        TypedQuery<Task> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByFinishDate(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("finishDate")));
        TypedQuery<Task> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByStatus(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("status")));
        TypedQuery<Task> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> sortAllByCreationDate(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(cb.equal(rootEntry.get("user").get("id"), userId));
        cq.orderBy(cb.asc(rootEntry.get("creationTime")));
        TypedQuery<Task> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> searchByName(@NotNull final String userId, @NotNull final String string) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.like(rootEntry.get("name"), "%" + string + "%")
        );
        TypedQuery<Task> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

    @NotNull
    @Override
    public Collection<Task> searchByDescription(@NotNull final String userId, @NotNull final String string) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Task> cq = cb.createQuery(Task.class);
        @NotNull final Root<Task> rootEntry = cq.from(Task.class);
        cq.where(
                cb.equal(rootEntry.get("user").get("id"), userId),
                cb.like(rootEntry.get("description"), "%" + string + "%")
        );
        TypedQuery<Task> allQuery = entityManager.createQuery(cq);
        return allQuery.getResultList();
    }

}