package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.ISessionRepository;
import ru.fadeev.tm.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class SessionRepository implements ISessionRepository {

    @NotNull
    private EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Nullable
    public Session findOne(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public void persist(@NotNull final Session session) {
        entityManager.persist(session);
    }

    @Override
    public void merge(@NotNull final Session session) {
        entityManager.merge(session);
    }

    @Override
    public void removeAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Session> cq = cb.createCriteriaDelete(Session.class);
        entityManager.createQuery(cq).executeUpdate();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Session> cq = cb.createQuery(Session.class);
        @NotNull final Root<Session> rootEntry = cq.from(Session.class);
        @NotNull final CriteriaQuery<Session> all = cq.select(rootEntry);
        TypedQuery<Session> allQuery = entityManager.createQuery(all);
        return allQuery.getResultList();
    }

    @Override
    public void remove(@NotNull final String id) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Session> cq = cb.createCriteriaDelete(Session.class);
        @NotNull final Root<Session> rootEntry = cq.from(Session.class);
        cq.where(cb.equal(rootEntry.get("id"), id));
        entityManager.createQuery(cq).executeUpdate();
    }

    @Nullable
    @Override
    public Session findByUserId(@NotNull final String userId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Session> cq = cb.createQuery(Session.class);
        @NotNull final Root<Session> rootEntry = cq.from(Session.class);
        @NotNull final CriteriaQuery<Session> query = cq.select(rootEntry);
        cq.where(cb.equal(rootEntry.get("userId"), userId));
        TypedQuery<Session> allQuery = entityManager.createQuery(query);
        return allQuery.getResultStream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeSessionBySignature(@NotNull final String signature) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaDelete<Session> cq = cb.createCriteriaDelete(Session.class);
        @NotNull final Root<Session> rootEntry = cq.from(Session.class);
        cq.where(cb.equal(rootEntry.get("signature"), signature));
        entityManager.createQuery(cq).executeUpdate();
    }

    @Override
    public boolean contains(@NotNull final String sessionId) {
        @NotNull final CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<Session> cq = cb.createQuery(Session.class);
        @NotNull final Root<Session> rootEntry = cq.from(Session.class);
        @NotNull final CriteriaQuery<Session> query = cq.select(rootEntry);
        cq.where(cb.equal(rootEntry.get("id"), sessionId));
        TypedQuery<Session> allQuery = entityManager.createQuery(query);
        return allQuery.getResultList().size() > 0;
    }

}
