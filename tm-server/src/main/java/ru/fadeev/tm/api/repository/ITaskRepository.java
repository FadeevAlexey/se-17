package ru.fadeev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskRepository {

    @NotNull
    Collection<Task> findAllByUserId(@NotNull String userId);

    @NotNull
    Collection<Task> findAllByProjectId(
            @NotNull String projectId,
            @NotNull String userId
    );

    @NotNull
    List<Task> findAll();

    @Nullable
    Task findOne(@NotNull String id);

    @Nullable
    Task findOneByUserId(
            @NotNull String userId,
            @NotNull String id
    );

    void remove(@NotNull String id);

    void persist(@NotNull Task task);

    void merge(@NotNull Task task);

    void removeAll();

    void removeAllByUserId(@NotNull String userId);

    @Nullable
    String findIdByName(
            @NotNull String userId,
            @NotNull String name
    );

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    );

    void removeAllProjectTask(@NotNull String userId);

    @NotNull
    Collection<Task> searchByName(
            @NotNull String userId,
            @NotNull String string
    );

    @NotNull
    Collection<Task> searchByDescription(
            @NotNull String userId,
            @NotNull String string);

    void removeByIdTask(
            @NotNull String userId,
            @NotNull String id
    );

    @NotNull
    Collection<Task> sortAllByStartDate(@NotNull String userId);

    @NotNull
    Collection<Task> sortAllByFinishDate(@NotNull String userId);

    @NotNull
    Collection<Task> sortAllByStatus(@NotNull String userId);

    @NotNull
    Collection<Task> sortAllByCreationDate(@NotNull String userId);

}