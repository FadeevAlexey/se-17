package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    List<UserDTO> findAllUser(@WebParam(name = "token") String token) throws Exception;

    @WebMethod
    @Nullable UserDTO findOneUser(
            @WebParam(name = "token") String token
    ) throws Exception;

    @WebMethod
    @Nullable UserDTO removeUser(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void persistUser(@WebParam(name = "user") @Nullable UserDTO user) throws Exception;

    @WebMethod
     void mergeUserAdmin(
            @WebParam(name = "token") String token,
            @WebParam(name = "user") @Nullable UserDTO user
    ) throws Exception;

    @WebMethod
    void mergeUser(
            @WebParam(name = "token") String token,
            @WebParam(name = "user") @Nullable UserDTO user
    ) throws Exception;

    @WebMethod
    boolean isLoginExistUser(
            @WebParam(name = "login") @Nullable String login) throws Exception;

    @Nullable
    @WebMethod
    UserDTO findUserByLoginUser(
            @WebParam(name = "token") String token,
            @WebParam(name = "login") @Nullable String login
    ) throws Exception;

}