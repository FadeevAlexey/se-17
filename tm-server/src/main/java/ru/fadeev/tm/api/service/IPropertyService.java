package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    void init() throws Exception;

    @Nullable
    String getServerPort();

    @Nullable
    String getServerHost();

    @Nullable
    String getSessionSalt();

    int getSessionCycle();

    int getSessionLifetime();

    @Nullable
    String getDbUrl() throws Exception;

    @NotNull
    String getJdbcDriver() throws Exception;

    @NotNull
    String getJdbcPassword() throws Exception;

    @NotNull
    String getJdbcUserName() throws Exception;

    @NotNull
    String getJdbcUrl() throws Exception;

    @NotNull
    String getSecretKey() throws Exception;

    @NotNull
    String getBindAddress() throws Exception;

}
