package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.SessionDTO;
import ru.fadeev.tm.entity.Session;
import ru.fadeev.tm.enumerated.Role;

public interface ISessionService extends IService<Session> {

    void closeSession(@Nullable SessionDTO session) throws Exception;

    boolean contains(@Nullable String sessionId) throws Exception;

    @NotNull
    SessionDTO checkSession(@Nullable final SessionDTO currentSession) throws Exception;

    @NotNull
    SessionDTO checkSession(SessionDTO currentSession, @NotNull final Role role) throws Exception;

    @Nullable
    Session openSession(@Nullable final String login, @Nullable String password) throws Exception;

}