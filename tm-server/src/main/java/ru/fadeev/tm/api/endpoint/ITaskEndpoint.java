package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface ITaskEndpoint {

    @Nullable
    @WebMethod
    TaskDTO findOneTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO removeTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void persistTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "task") @NotNull TaskDTO task
    ) throws Exception;

    @WebMethod
    void mergeTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "task") @NotNull TaskDTO task
    ) throws Exception;


    @Nullable
    @WebMethod
    String findIdByNameTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDTO> findAllTask(@WebParam(name = "token") String token) throws Exception;

    @WebMethod
    void removeAllTask(@WebParam(name = "token") String token) throws Exception;
    
    @NotNull
    @WebMethod
    Collection<TaskDTO> searchByNameTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDTO> searchByDescriptionTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDTO> findAllByProjectIdTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @WebMethod
    void removeAllByProjectIdTask(
            @WebParam(name = "token") String token,
            @WebParam(name = "projectId") @Nullable String projectId
    ) throws Exception;

    @WebMethod
    void removeAllProjectTask(@WebParam(name = "token") String token) throws Exception;
    
    @NotNull
    @WebMethod
    Collection<TaskDTO> sortByStartDateTask(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDTO> sortByFinishDateTask(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDTO> sortByCreationTimeTask(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<TaskDTO> sortByStatusTask(@WebParam(name = "token") final String token) throws Exception;

}