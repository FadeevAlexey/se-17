package ru.fadeev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProjectAdmin(@WebParam(name = "session") String session) throws Exception;

    @Nullable
    @WebMethod
    ProjectDTO findOneProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @Nullable
    @WebMethod
     void removeProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "id") @Nullable String id
    ) throws Exception;

    @WebMethod
    void persistProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "project") @NotNull ProjectDTO project
    ) throws Exception;

    @WebMethod
    void mergeProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "project") @Nullable ProjectDTO project
    ) throws Exception;

    @WebMethod
    void removeAllProjectAdmin(
            @WebParam(name = "token") String token
    ) throws Exception;

    @Nullable
    @WebMethod
    String findIdByNameProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "name") @Nullable String name
    ) throws Exception;

    @NotNull
    @WebMethod
    List<ProjectDTO> findAllProject(@WebParam(name = "token") String token) throws Exception;

    @WebMethod
    void removeAllProject(
            @WebParam(name = "token") String token) throws Exception;


    @NotNull
    @WebMethod
    Collection<ProjectDTO> searchByNameProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDTO> searchByDescriptionProject(
            @WebParam(name = "token") String token,
            @WebParam(name = "string") @Nullable String string
    ) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDTO> sortByStartDateProject(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDTO> sortByFinishDateProject(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDTO> sortByCreationTimeProject(@WebParam(name = "token") String token) throws Exception;

    @NotNull
    @WebMethod
    Collection<ProjectDTO> sortByStatusProject(@WebParam(name = "token") final String token) throws Exception;

}