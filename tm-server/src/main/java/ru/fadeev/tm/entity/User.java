package ru.fadeev.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.enumerated.Role;
import ru.fadeev.tm.listener.EntityListener;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlType
@Cacheable
@NoArgsConstructor
@Entity(name = "app_user")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@JsonIgnoreProperties(value= {"tasks", "projects", "sessions"})
public final class User extends AbstractEntity {

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Session> sessions = new ArrayList<>();

    @Nullable
    @Column(unique = true, nullable = false)
    private String login = "";

    @Nullable
    @Column(name = "password_hash")
    private String passwordHash = "";

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Role role = Role.USER;

}