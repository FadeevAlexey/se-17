package ru.fadeev.tm.constant;

public class Sort {

    public static final String SUFFIX = "-DESC";

    public static final String SORT_ASC = "ASC";

    public static final String SORT_DESC = "DESC";

}