package ru.fadeev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.exception.SecureException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public class EncryptUtil {

    @Nullable
    private static SecretKeySpec secretKey;

    @Nullable
    private static byte[] key;

    private static void setKey(@NotNull final String myKey) throws Exception {
        @Nullable MessageDigest sha = null;
        key = myKey.getBytes("UTF-8");
        sha = MessageDigest.getInstance("SHA-1");
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);
        secretKey = new SecretKeySpec(key, "AES");
    }

    @NotNull
    public static String encrypt(@Nullable final String strToEncrypt, @Nullable final String secret) throws Exception {
        if (strToEncrypt == null || strToEncrypt.isEmpty()) throw new SecureException("Encrypt Exception");
        if (secret == null || secret.isEmpty()) throw new SecureException("Encrypt Exception");
        setKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        @NotNull final byte[] data = cipher.doFinal(strToEncrypt.getBytes("UTF-8"));
        return Base64.getEncoder().encodeToString(data);
    }

    public static String decrypt(@Nullable final String strToDecrypt, @Nullable final String secret) throws Exception {
        if (strToDecrypt == null || strToDecrypt.isEmpty()) throw new SecureException("Decrypt Exception");
        if (secret == null || secret.isEmpty()) throw new SecureException("Decrypt Exception");
        setKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        @NotNull final byte[] data = Base64.getDecoder().decode(strToDecrypt);
        return new String(cipher.doFinal(data));
    }

}