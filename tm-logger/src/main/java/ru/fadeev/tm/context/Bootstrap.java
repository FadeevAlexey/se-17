package ru.fadeev.tm.context;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import ru.fadeev.tm.listener.LoggerListener;

import javax.jms.*;

public final class Bootstrap {

    private final String brokerUrl = ActiveMQConnection.DEFAULT_BROKER_URL;

    @SneakyThrows
    public void init() {
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(brokerUrl);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        addBroadcastConsumer(session, "Project");
        addBroadcastConsumer(session, "Task");
        addBroadcastConsumer(session, "Session");
        addBroadcastConsumer(session, "User");
    }

    @SneakyThrows
    private void addBroadcastConsumer(@NotNull final Session session, @NotNull final String topicName) {
        @NotNull final Destination destination = session.createTopic(topicName);
        @NotNull final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(new LoggerListener());
    }

}