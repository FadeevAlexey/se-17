package ru.fadeev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.endpoint.TaskDTO;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;

public interface ITerminalService {

    @Nullable
    String readString();

    @Nullable
    XMLGregorianCalendar readDate() throws Exception;

    void println(@Nullable String string);

    void print(@Nullable String string);

    void println(@Nullable TaskDTO task);

    void println(@Nullable ProjectDTO project);

    void printTaskList(@Nullable Collection<TaskDTO> tasks);

    void printProjectList(@Nullable Collection<ProjectDTO> projects);

}
