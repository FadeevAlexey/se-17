package ru.fadeev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.repository.IAppStateRepository;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.entity.AppState;

import java.util.ArrayList;
import java.util.List;

public final class AppStateRepository implements IAppStateRepository {

    @NotNull
    private final AppState appState = new AppState();

    @Override
    public void putCommand(@Nullable final String description, @Nullable final AbstractCommand abstractCommand) {
        appState.getCommands().put(description, abstractCommand);
    }

    @Nullable
    public AbstractCommand getCommand(@Nullable final String command) {
        return appState.getCommands().get(command);
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(appState.getCommands().values());
    }

    @Nullable
    public String getToken(){
        return appState.getToken();
    }

    public void setToken(@Nullable final String token){
        appState.setToken(token);
    }

}