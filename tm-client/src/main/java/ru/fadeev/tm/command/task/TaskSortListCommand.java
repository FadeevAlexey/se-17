package ru.fadeev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.TaskDTO;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.exception.AccessDeniedException;

import java.util.Collection;
import java.util.Collections;

public final class TaskSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sortList";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Shows sorted tasks by START DATE, FINISH DATE, STATUS.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[TASK SORT LIST]");
        terminal.println("Select sorting type:\n" +
                "START DATE, FINISH DATE, STATUS or press ENTER for default sort by adding");
        @Nullable final String sortRequest = terminal.readString();
        terminal.printTaskList(sortAll(token, sortRequest));
    }

    public Collection<TaskDTO> sortAll(@Nullable final String token, @Nullable String sortRequest) throws Exception {
        if (token == null || token.isEmpty()) return Collections.emptyList();
        if (sortRequest == null || sortRequest.isEmpty())
            return serviceLocator.getTaskEndpoint().sortByCreationTimeTask(token);
        if (sortRequest.toUpperCase().contains(Sort.SUFFIX)) {
            sortRequest = sortRequest.substring(0, sortRequest.length() - Sort.SUFFIX.length());
        }
        switch (sortRequest.toLowerCase()) {
            case "start date":
                return serviceLocator.getTaskEndpoint().sortByStartDateTask(token);
            case "finish date":
                return serviceLocator.getTaskEndpoint().sortByFinishDateTask(token);
            case "status":
                return serviceLocator.getTaskEndpoint().sortByStatusTask(token);
            default:
                throw new Exception("cannot sort");
        }
    }

}