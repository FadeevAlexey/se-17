package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.IProjectEndpoint;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.exception.AccessDeniedException;
import ru.fadeev.tm.exception.IllegalSearchRequestException;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public final class ProjectSearchCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-search";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Search for projects by NAME or DESCRIPTION.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT SEARCH]");
        terminal.println("SEARCH REQUEST");
        @Nullable final String searchRequest = terminal.readString();
        if (searchRequest == null || searchRequest.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.println("Select search type: NAME, DESCRIPTION, ALL");
        @Nullable final String typeSearch = terminal.readString();
        if (typeSearch == null || typeSearch.isEmpty())
            throw new IllegalSearchRequestException("Search request can't be empty");
        terminal.printProjectList(getProjectList(token, searchRequest, typeSearch)
        );
    }

    @NotNull
    private Collection<ProjectDTO> getProjectList(
            @NotNull final String  session,
            @NotNull final String searchRequest,
            @NotNull final String typeSearch
    ) throws Exception {
        @NotNull final IProjectEndpoint projectEndpoint = serviceLocator.getProjectEndpoint();
        switch (typeSearch.toLowerCase()) {
            case "name":
                return projectEndpoint.searchByNameProject(session, searchRequest);
            case "description":
                return projectEndpoint.searchByDescriptionProject(session, searchRequest);
            case "all": {
                @NotNull final Collection<ProjectDTO> searchByName =
                        projectEndpoint.searchByNameProject(session, searchRequest);
                @NotNull final Collection<ProjectDTO> searchByDescription =
                        projectEndpoint.searchByDescriptionProject(session, searchRequest);
                @NotNull final Set<ProjectDTO> allResult = new HashSet<>();
                allResult.addAll(searchByName);
                allResult.addAll(searchByDescription);
                return allResult;
            }
            default:
                throw new IllegalSearchRequestException("invalid search type");
        }
    }

}