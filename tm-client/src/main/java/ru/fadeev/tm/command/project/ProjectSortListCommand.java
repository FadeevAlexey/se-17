package ru.fadeev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.fadeev.tm.api.endpoint.ProjectDTO;
import ru.fadeev.tm.api.service.ITerminalService;
import ru.fadeev.tm.command.AbstractCommand;
import ru.fadeev.tm.constant.Sort;
import ru.fadeev.tm.exception.AccessDeniedException;

import java.util.Collection;
import java.util.Collections;

public final class ProjectSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-sortList";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Shows sorted projects by START DATE, FINISH DATE, STATUS.";
    }

    @Override
    public void execute() throws Exception {
        @NotNull final ITerminalService terminal = serviceLocator.getTerminalService();
        @Nullable final String token = serviceLocator.getAppStateService().getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException("Access denied");
        terminal.println("[PROJECT SORT LIST]");
        terminal.println("Select sorting type:\n" +
                "START DATE, FINISH DATE, STATUS or press ENTER for default sort by adding\n" +
                "if you'd like descending sort use suffix "+ Sort.SUFFIX +", for example: STATUS" +
                Sort.SUFFIX +"\n");
        @Nullable final String sortRequest = terminal.readString();
        terminal.printProjectList(sortAll(token, sortRequest));
    }

    public Collection<ProjectDTO> sortAll(@Nullable final String token, @Nullable String sortRequest) throws Exception {
        if (token == null || token.isEmpty()) return Collections.emptyList();
        if (sortRequest == null || sortRequest.isEmpty())
            return  serviceLocator.getProjectEndpoint().sortByCreationTimeProject(token);
        if (sortRequest.toUpperCase().contains(Sort.SUFFIX)) {
            sortRequest = sortRequest.substring(0, sortRequest.length() - Sort.SUFFIX.length());
        }
        switch (sortRequest.toLowerCase()) {
            case "start date":
                return serviceLocator.getProjectEndpoint().sortByStartDateProject(token);
            case "finish date":
                return serviceLocator.getProjectEndpoint().sortByFinishDateProject(token);
            case "status":
                return serviceLocator.getProjectEndpoint().sortByStatusProject(token);
            default:
                throw new Exception("cannot sort");
        }
    }

}