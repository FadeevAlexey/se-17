package ru.fadeev.tm;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.fadeev.tm.api.endpoint.*;
import ru.fadeev.tm.constant.TestConst;
import ru.fadeev.tm.endpoint.*;

import java.lang.Exception;

@Getter
@Setter
@Category(IntegrationTest.class)
public abstract class AbstractTest {

    @Nullable
    protected String tokenUser;

    @Nullable
    protected String tokenAdmin;

    @Nullable
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    protected ISessionEndpoint sessionEndpoint;

    @Nullable
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    protected IUserEndpoint userEndpoint;

    @Before
    public void setUp() throws Exception {
        projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
        sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
        userEndpoint = new UserEndpointService().getUserEndpointPort();
        tokenUser = sessionEndpoint.getToken(TestConst.USER_LOGIN, TestConst.USER_PASSWORD);
        tokenAdmin = sessionEndpoint.getToken(TestConst.ADMIN_LOGIN, TestConst.ADMIN_PASSWORD);
    }

    @After
    public void tearDown() throws Exception {
        taskEndpoint.removeAllTask(tokenUser);
        taskEndpoint.removeAllTask(tokenAdmin);
        projectEndpoint.removeAllProject(tokenUser);
        projectEndpoint.removeAllProject(tokenAdmin);
        sessionEndpoint.closeSession(tokenUser);
        sessionEndpoint.closeSession(tokenAdmin);
    }

}